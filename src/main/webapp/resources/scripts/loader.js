/**
 * Created by HaidarZ on 2/20/17.
 */
var loaderModule = (function() {

    var divContent;
    function showLoading(div) {
        // check if the DIV already contains the loader DIV
        if($(div).find(".spinner").length > 0){
            return;
        }
        divContent = $(div).html();
        // set loader as content for this DIV
        $(div).html($('#loader').html());
        $('#loader').attr('display', 'block');
    }

    function hideLoading(div) {
        // hide loading
        $('#loader').attr('display', 'none');
        // return DIV content
        $(div).html(divContent);
    }

    function showStoryLoading(div) {
        // check if the DIV already contains the loader DIV
        if($(div).find('.spinner-story').length > 0){
            return;
        }
        // set loader as content for this DIV
        var loaderHtml = '<div class="spinner-story"><div class="bounce1" /><div class="bounce2" /><div class="bounce3" /></div>';
        $(div).html(loaderHtml);
    }

    function hideStoryLoading(div) {
        $(div).html('');
    }

    // Mini loader
    function showMiniLoading(div) {
        // set loader as content for this DIV
        $(div).html($('#loader').html());
        $('#loader').attr('display', 'block');
    }

    // Blur loader
    function showBlurLoader(div) {
        $('#blurLoader').css('display', 'block');
        var pos = $(div).offset();
        $(div).css('position', 'relative');
        // set loader as content for this DIV

        $('#blurLoader').css({
            "left" : pos.left ,
            "top" : pos.top
        });
    }

    function hideBlurLoader(div) {
        $(div).parent().find('#blurLoader').remove();
        $('#blurLoader').css('display', 'none');
    }

    function showFullPageLoader(){
        $("#fullPageLoader").css('height', $(document).height());
        $("#fullPageLoader").show();
    }

    function hideFullPageLoader() {
        $("#fullPageLoader").hide();
    }

    // Reveal public pointers to private functions and properties
    return {
        showLoading : showLoading,
        hideLoading : hideLoading,
        showMiniLoading : showMiniLoading,
        showBlurLoader : showBlurLoader,
        hideBlurLoader : hideBlurLoader,
        showFullPageLoader : showFullPageLoader,
        hideFullPageLoader : hideFullPageLoader,
        showStoryLoading : showStoryLoading,
        hideStoryLoading : hideStoryLoading
    };
})();
