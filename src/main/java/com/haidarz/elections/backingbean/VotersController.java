package com.haidarz.elections.backingbean;

import com.haidarz.elections.backingbean.util.MobilePageController;
import com.haidarz.elections.entities.*;
import com.haidarz.elections.facade.JudiciaryFacade;
import com.haidarz.elections.facade.ProvinceFacade;
import com.haidarz.elections.facade.ReligionFacade;
import com.haidarz.elections.facade.VillageFacade;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@Named(value = "votersController")
@ViewScoped
public class VotersController extends AbstractController<Voters> {

    @Inject
    private MobilePageController mobilePageController;

    @Inject
    private ReligionFacade religionFacade;

    @Inject
    private ProvinceFacade provinceFacade;

    @Inject
    private JudiciaryFacade judiciaryFacade;

    @Inject
    private VillageFacade villageFacade;

    private String province;

    private String judiciary;

    private String village;

    private List<Province> provinces;

    private List<Judiciary> judiciaries;

    private List<Villages> villages;


    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    public List<Judiciary> getJudiciaries() {
        return judiciaries;
    }

    public void setJudiciaries(List<Judiciary> judiciaries) {
        this.judiciaries = judiciaries;
    }

    public List<Villages> getVillages() {
        return villages;
    }

    public void setVillages(List<Villages> villages) {
        this.villages = villages;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getJudiciary() {
        return judiciary;
    }

    public void setJudiciary(String judiciary) {
        this.judiciary = judiciary;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public List<String> getSex() {
        List<String> sexList = new ArrayList<>();
        sexList.add("ذكر");
        sexList.add("اناث");
        return sexList;
    }

    public List<Religion> getReligions() {
        return religionFacade.findAll();
    }

    public VotersController() {
        // Inform the Abstract parent controller of the concrete Voters Entity
        super(Voters.class);
    }

    public void onProvinceChange() {
        if (null != province && !province.equals("")) {
            //update judiciaries and villages
            judiciaries = judiciaryFacade.findJudiciaryWhereProvince(province);
            villages = villageFacade.findVillagesWhereProvince(province);
            judiciary = null;
            village = null;
        }
    }

    public void onJudiciaryChange() {
        if (null != judiciary && !judiciary.equals("")) {
            //update villages
            villages = villageFacade.findVillagesWhereJudiciary(judiciary);
            village = null;
        }
    }

    @PostConstruct
    public void init() {
        provinces = provinceFacade.findAll();
        judiciaries = judiciaryFacade.findAll();
        villages = villageFacade.findAll();
    }


}
