package com.haidarz.elections.facade;

import com.haidarz.elections.entities.Villages;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaidarZ on 2/19/17.
 */
@Stateless
public class VillageFacade extends AbstractFacade<Villages> {

    @PersistenceContext(unitName = "com.haidarz_elections-web_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VillageFacade() {
        super(Villages.class);
    }

    public List<Villages> findVillagesWhereProvince(String province) {
        List<Villages> villagesList = new ArrayList<>();
        if (null != province && !province.equals("")) {
            villagesList = getEntityManager()
                    .createQuery("Select v from Villages v where v.province = :province")
                    .setParameter("province", province)
                    .getResultList();
        }
        return villagesList;
    }

    public List<Villages> findVillagesWhereJudiciary(String judiciary) {
        List<Villages> villagesList = new ArrayList<>();
        if (null != judiciary && !judiciary.equals("")) {
            villagesList = getEntityManager()
                    .createQuery("Select v from Villages v where v.judiciary = :judiciary")
                    .setParameter("judiciary", judiciary)
                    .getResultList();
        }
        return villagesList;
    }

}
