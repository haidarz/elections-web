/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.haidarz.elections.facade;

import com.haidarz.elections.entities.Voters;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author HaidarZ
 */
@Stateless
public class VotersFacade extends AbstractFacade<Voters> {

    @PersistenceContext(unitName = "com.haidarz_elections-web_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VotersFacade() {
        super(Voters.class);
    }
    
}
