package com.haidarz.elections.facade;

import com.haidarz.elections.entities.Religion;
import com.haidarz.elections.entities.Voters;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by HaidarZ on 2/19/17.
 */
@Stateless
public class ReligionFacade extends AbstractFacade<Religion> {

    @PersistenceContext(unitName = "com.haidarz_elections-web_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReligionFacade() {
        super(Religion.class);
    }
}
