package com.haidarz.elections.facade;

import com.haidarz.elections.entities.Judiciary;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaidarZ on 2/19/17.
 */
@Stateless
public class JudiciaryFacade extends AbstractFacade<Judiciary> {

    @PersistenceContext(unitName = "com.haidarz_elections-web_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JudiciaryFacade() {
        super(Judiciary.class);
    }

    public List<Judiciary> findJudiciaryWhereProvince(String province) {
        List<Judiciary> judiciaryList = new ArrayList<>();
        if (null != province && !province.equals("")) {
            judiciaryList = getEntityManager()
                    .createQuery("Select v from Judiciary v where v.province = :province")
                    .setParameter("province", province)
                    .getResultList();
        }
        return judiciaryList;
    }

}
