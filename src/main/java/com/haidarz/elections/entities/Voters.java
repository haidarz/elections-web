/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.haidarz.elections.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HaidarZ
 */
@Entity
@Table(name = "Voters")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Voters.findAll", query = "SELECT v FROM Voters v")
    , @NamedQuery(name = "Voters.findById", query = "SELECT v FROM Voters v WHERE v.id = :id")
    , @NamedQuery(name = "Voters.findByFirstName", query = "SELECT v FROM Voters v WHERE v.firstName = :firstName")
    , @NamedQuery(name = "Voters.findByMiddleName", query = "SELECT v FROM Voters v WHERE v.middleName = :middleName")
    , @NamedQuery(name = "Voters.findByLastName", query = "SELECT v FROM Voters v WHERE v.lastName = :lastName")
    , @NamedQuery(name = "Voters.findByMotherName", query = "SELECT v FROM Voters v WHERE v.motherName = :motherName")
    , @NamedQuery(name = "Voters.findByBirthDate", query = "SELECT v FROM Voters v WHERE v.birthDate = :birthDate")
    , @NamedQuery(name = "Voters.findBySex", query = "SELECT v FROM Voters v WHERE v.sex = :sex")
    , @NamedQuery(name = "Voters.findByRegistrationNb", query = "SELECT v FROM Voters v WHERE v.registrationNb = :registrationNb")
    , @NamedQuery(name = "Voters.findByListReligion", query = "SELECT v FROM Voters v WHERE v.listReligion = :listReligion")
    , @NamedQuery(name = "Voters.findByReligion", query = "SELECT v FROM Voters v WHERE v.religion = :religion")
    , @NamedQuery(name = "Voters.findByProvince", query = "SELECT v FROM Voters v WHERE v.province = :province")
    , @NamedQuery(name = "Voters.findByJudiciary", query = "SELECT v FROM Voters v WHERE v.judiciary = :judiciary")
    , @NamedQuery(name = "Voters.findByVillage", query = "SELECT v FROM Voters v WHERE v.village = :village")})
public class Voters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 150)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 150)
    @Column(name = "middle_name")
    private String middleName;
    @Size(max = 150)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 150)
    @Column(name = "mother_name")
    private String motherName;
    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Size(max = 150)
    @Column(name = "sex")
    private String sex;
    @Column(name = "registration_nb")
    private Integer registrationNb;
    @Size(max = 150)
    @Column(name = "list_religion")
    private String listReligion;
    @Size(max = 150)
    @Column(name = "religion")
    private String religion;
    @Size(max = 150)
    @Column(name = "province")
    private String province;
    @Size(max = 150)
    @Column(name = "judiciary")
    private String judiciary;
    @Size(max = 150)
    @Column(name = "village")
    private String village;

    public Voters() {
    }

    public Voters(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getRegistrationNb() {
        return registrationNb;
    }

    public void setRegistrationNb(Integer registrationNb) {
        this.registrationNb = registrationNb;
    }

    public String getListReligion() {
        return listReligion;
    }

    public void setListReligion(String listReligion) {
        this.listReligion = listReligion;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getJudiciary() {
        return judiciary;
    }

    public void setJudiciary(String judiciary) {
        this.judiciary = judiciary;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voters)) {
            return false;
        }
        Voters other = (Voters) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.haidarz.elections.entities.Voters[ id=" + id + " ]";
    }
    
}
