/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.haidarz.elections.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HaidarZ
 */
@Entity
@Table(name = "judiciary")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Judiciary.findAll", query = "SELECT j FROM Judiciary j")
    , @NamedQuery(name = "Judiciary.findById", query = "SELECT j FROM Judiciary j WHERE j.id = :id")
    , @NamedQuery(name = "Judiciary.findByName", query = "SELECT j FROM Judiciary j WHERE j.name = :name")
    , @NamedQuery(name = "Judiciary.findByProvince", query = "SELECT j FROM Judiciary j WHERE j.province = :province")})
public class Judiciary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 250)
    @Column(name = "name")
    private String name;
    @Size(max = 250)
    @Column(name = "province")
    private String province;

    public Judiciary() {
    }

    public Judiciary(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Judiciary)) {
            return false;
        }
        Judiciary other = (Judiciary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.haidarz.elections.entities.Judiciary[ id=" + id + " ]";
    }
    
}
