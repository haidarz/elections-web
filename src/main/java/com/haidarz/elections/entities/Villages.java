/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.haidarz.elections.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HaidarZ
 */
@Entity
@Table(name = "villages")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Villages.findAll", query = "SELECT v FROM Villages v")
    , @NamedQuery(name = "Villages.findById", query = "SELECT v FROM Villages v WHERE v.id = :id")
    , @NamedQuery(name = "Villages.findByName", query = "SELECT v FROM Villages v WHERE v.name = :name")
    , @NamedQuery(name = "Villages.findByProvince", query = "SELECT v FROM Villages v WHERE v.province = :province")
    , @NamedQuery(name = "Villages.findByJudiciary", query = "SELECT v FROM Villages v WHERE v.judiciary = :judiciary")})
public class Villages implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 250)
    @Column(name = "name")
    private String name;
    @Size(max = 250)
    @Column(name = "province")
    private String province;
    @Size(max = 250)
    @Column(name = "judiciary")
    private String judiciary;

    public Villages() {
    }

    public Villages(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getJudiciary() {
        return judiciary;
    }

    public void setJudiciary(String judiciary) {
        this.judiciary = judiciary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Villages)) {
            return false;
        }
        Villages other = (Villages) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.haidarz.elections.entities.Villages[ id=" + id + " ]";
    }
    
}
