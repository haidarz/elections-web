package com.haidarz.elections.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.4.v20160829-rNA", date="2017-02-21T12:31:28")
@StaticMetamodel(Villages.class)
public class Villages_ { 

    public static volatile SingularAttribute<Villages, String> province;
    public static volatile SingularAttribute<Villages, String> name;
    public static volatile SingularAttribute<Villages, Integer> id;
    public static volatile SingularAttribute<Villages, String> judiciary;

}