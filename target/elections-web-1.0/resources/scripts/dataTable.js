var dataTableModule = (function () {

    function init() {
        $(".datatable").find(".ui-datatable-tablewrapper").doubleScroll();
    }

    function onPagerStart() {
        loaderModule.showBlurLoader(".dataTables_wrapper");
    }

    function onPagerComplete() {
        loaderModule.hideBlurLoader(".dataTables_wrapper");
    }

    function onPagerError() {
        loaderModule.hideBlurLoader(".dataTables_wrapper");
    }

    return {
        init: init,
        onPagerStart: onPagerStart,
        onPagerComplete: onPagerComplete,
        onPagerError: onPagerError
    };
})();
$(function () {
    dataTableModule.init();
});